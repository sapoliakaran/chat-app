import React from 'react';
import Chatkit from '@pusher/chatkit-client';
import MessageList from './components/MessageList';
import SendMessageForm from './components/SendMessageForm';
import RoomList from './components/RoomList';
import NewRoomForm from './components/NewRoomForm';
import Login from './components/Login';
import {tokenUrl, instanceLocator} from './config';

class App extends React.Component {

    constructor() {
        super();

        this.state = {
            currentScreen: 'login',
            username: '',
            roomId: null,
            messages: [],
            joinableRooms: [],
            joinedRooms: []
        };
        
        this.sendMessage = this.sendMessage.bind(this);
        this.subscribeToRoom = this.subscribeToRoom.bind(this);
        this.getRooms = this.getRooms.bind(this);
        this.createRoom = this.createRoom.bind(this);
    }

    //componentDidMount() {
    //    const chatManager = new Chatkit.ChatManager({
    //        instanceLocator,tokenUrl
    //        userId: 'zedd',
    //        tokenProvider: new Chatkit.TokenProvider({
    //            url: tokenUrl
//
    //        })
    //    });
//
    //    chatManager.connect()
    //    .then(currentUser => {
    //        this.currentUser = currentUser;
    //        this.getRooms();
    //    });
    //}

    updateUsername = username => {
        this.setState({
            username: username.target.value
        });
        console.log("Current username: ", this.state.username);
    };

    enterChat = () => {
        this.chatManager = new Chatkit.ChatManager({
            instanceLocator,
            userId: this.state.username,
            tokenProvider: new Chatkit.TokenProvider({
                url: tokenUrl
            })
        });
        this.chatManager
            .connect()
            .then(currentUser => {
                this.currentUser = currentUser;
                console.log('Current user: ', currentUser);
                this.subscribeToRoom('30024215');
            })
        .catch(err => {
            console.log('Error on user registration', err);
            //let chatkit = new Chatkit.default({
            //    instanceLocator,
            //    key: '51e0c9b8-9aa2-4e26-8d8c-0b440d2de55d:f1SKYWHUOHg3qbPIxjOzUWTOjIGJXkHF6rX5GUo4zaw='
    //
            //});

            this.chatManager.createUser({
                id: this.state.username
            });

            this.chatManager
            .connect()
            .then(currentUser => {
                this.currentUser = currentUser;
                console.log('Current user: ', currentUser);
                this.subscribeToRoom('30024215');
            });
        });
    }

    getRooms() {
        this.currentUser.getJoinableRooms()
        .then(joinableRooms => {
            this.setState({
                joinableRooms: joinableRooms,
                joinedRooms: this.currentUser.rooms
            })
        })
        .catch(err => console.log('Error on joinableRooms', err));
    }

    subscribeToRoom(id) {
        this.setState({ 
            messages: [],
            currentScreen: id
        });
        this.currentUser.subscribeToRoomMultipart({
            roomId: id,
            hooks: {
                onMessage: message => {
                    //console.log('message.text: ', message.parts[0].payload.content);
                    this.setState({
                        messages: [...this.state.messages, message]
                    })
                }
            }
        })
        .then(room => {
            this.setState({
                roomId: room.id
            });
            this.getRooms()
        })
        .catch(err => console.log('Error in connecting to room', err));
    }

    createRoom(name) {
        this.currentUser.createRoom({
            name: name,
            private: false,
            addUserIds: ['karan']
        })
        .then(room => this.subscribeToRoom(room.id))
        .then( this.getRooms())
        .catch(err => console.log('Error in creating new room: ', err));
    }

    sendMessage(text, id) {
        this.currentUser.sendSimpleMessage({
            text: text,
            roomId: this.state.roomId
        })
    }

    render() {
        //console.log(...this.state.messages);
        {
            if(this.state.currentScreen === 'login') {
                return (
                    <Login 
                        username={this.state.username}
                        updateUsername={this.updateUsername}
                        enterChat={this.enterChat}
                    />
                );
            }
        } 
        {
            if(this.state.currentScreen !== 'login') {
                return(
                    <div className="app">
                        <RoomList 
                            roomId={this.state.roomId}
                            subscribeToRoom={this.subscribeToRoom} 
                            rooms={[...this.state.joinableRooms, ...this.state.joinedRooms]}
                        />
                        <MessageList
                            roomId={this.state.roomId}
                            messages={this.state.messages} 
                        />
                        <SendMessageForm 
                            disabled={!this.state.roomId}
                            sendMessage={this.sendMessage} 
                        />
                        <NewRoomForm createRoom={this.createRoom}/>
                    </div>
                );
            }
        } 
    }
}

export default App;