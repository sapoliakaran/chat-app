import React from 'react';

class Login extends React.Component {
    render() {
        return (   
            <div className="send-message-form">    
                <input
                    className="message"
                    onChange={this.props.updateUsername}
                    value={this.props.username}
                    placeholder="Enter username"
                    type="text"
                />
                <button 
                    className="message"
                    type="button"
                    onClick={this.props.enterChat}
                >
                    Enter
                </button>
            </div>
        );
    }
}

export default Login;