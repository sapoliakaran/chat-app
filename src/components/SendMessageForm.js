import React from 'react'

class SendMessageForm extends React.Component {
    constructor() {
        super();
        this.state = {
            input: ''
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        //console.log(e.target.value);
        this.setState({
            input: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.sendMessage(this.state.input);
        this.setState({
            input: ''
        });
    }

    render() {
        //console.log(this.state.input);
        return (
            <form 
                onSubmit={this.handleSubmit}
                className="send-message-form">
                <input
                    disabled={this.props.disabled}
                    onChange={this.handleChange}
                    value={this.state.input}
                    placeholder= { this.props.disabled ? "" : "Type message here and press ENTER"}
                    type="text" />
            </form>
        )
    }
}

export default SendMessageForm;