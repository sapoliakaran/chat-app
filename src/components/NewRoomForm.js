import React from 'react'

class NewRoomForm extends React.Component {
    constructor() {
        super();
        this.state = {
            input: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            input: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.createRoom(this.state.input);
        this.setState({
            input: ''
        });
        
    }

    render () {
        return (
            <div className="new-room-form">
                <form
                    onSubmit={this.handleSubmit}>
                    <input
                        onChange={this.handleChange}
                        value={this.state.input}
                        type="text" 
                        placeholder="Create new room" 
                        required />
                    <button id="create-room-btn" type="submit">+</button>
                </form>
            </div>
        )
    }
}

export default NewRoomForm