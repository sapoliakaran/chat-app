const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/a0853008-6114-4d16-a056-d17ba9014777/token";
const instanceLocator = "v1:us1:a0853008-6114-4d16-a056-d17ba9014777";

export { tokenUrl, instanceLocator }